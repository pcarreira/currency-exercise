class Graph {
	constructor() {
		this.targets = {}
	}

	addBidirectionalRate(source, target, rateValue) {
		this.addRate(source, target, rateValue);
		this.addRate(target, source, 1 / rateValue);
	};

	addRate(source, target, rateValue) {
		if (!this.targets[source]) {
			this.targets[source] = [];
		}
		this.targets[source].push({ currency: target, rateValue });
	}

	findPath(startNode, endNode, queue = [startNode]) {
		const lastNode = queue[queue.length - 1];
		const targets = this.targets[lastNode].filter(target => !queue.includes(target.currency));

		if (targets.length === 0) {
			return;
		}

		let path;
		for (let i = 0; i < targets.length; i++) {
			const target = targets[i];
			if (target.currency === endNode) {
				queue.push(target.currency);
				path = queue;
				break;
			} else {
				path = this.findPath(startNode, endNode, [...queue, target.currency]);
			}
		}

		return path;
	}

	calculateRate(startNode, endNode) {
		const path = this.findPath(startNode, endNode);
		if (!path) {
			return null;
		}

		let rate;
		for (let i = 1; i < path.length; i++) {
			const startNode = path[i - 1];
			const endNode = path[i];

			const target = this.targets[startNode];
			const indexRate = target.find(target => target.currency === endNode).rateValue;
			rate = rate ? rate * indexRate : indexRate;
		}
		return rate;
	}
}

module.exports = Graph;

