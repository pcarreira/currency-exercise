const Graph = require('./graph');

const rates = [
	["USD", "EUR", 0.9],
	["GBP", "USD", 1.29 ],
	["GBP", "JPY", 141.40 ],
];

function buildGraph() {
	const graph = new Graph();
	rates.forEach(rate => {
		graph.addBidirectionalRate(rate[0], rate[1], rate[2]);
	});
	return graph;
}

const graph = buildGraph();
const rate = graph.calculateRate('USD', 'EUR');

console.log(rate);